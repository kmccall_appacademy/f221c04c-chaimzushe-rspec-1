def echo(word)
  word
end

def shout(word)
   word.upcase
end

def repeat(word ,repeates=2)
  Array.new(repeates){word}.join(" ")
end


def start_of_word(word, stop)
  word.slice(0, stop)
end

def first_word(sentence)
   sentence.split(" ").first
end

def titleize(sentence)
  stop_words = ["over", "and", "the"]
  sentence.split(" ").map.with_index do|word, index|
    index != 0 && stop_words.include?(word) ? word : word.capitalize
  end.join(" ")

end
