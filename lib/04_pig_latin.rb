require 'byebug'
VOWELS = ["a","e", "i", "o", "u"]

def translate(sentence)
  sentence.split(" ").map{|word| latinize(word)}.join(" ")
end

def latinize(word)
  return word + "ay" if VOWELS.include?(word[0])
  word.chars.each_with_index do |char, index|
    if VOWELS.include?(char)
      index += 1 if char == "u" && word[index -1] == "q"
      return  word[index..-1] + word[0...index] + "ay"
    end
  end
  word + "ay"
end
