def add(n1, n2)
  n1 + n2
end

def subtract(n1, n2)
  n1 - n2
end

def sum(array)
   array.inject(&:+) || 0
end

def multiply(*array)
  array.reduce(&:*)
end

def power(n1 , n2)
  n1**n2
end

def factorial(n)
  (1..n).to_a.reduce(1){|product, number| product * number}
end
